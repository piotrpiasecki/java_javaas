package io.github.mat3e.lang;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "langs")
public class Lang
{
   @Id
   @GeneratedValue(generator = "increment")
   @GenericGenerator(name = "inc", strategy = "increment")
   private Integer id;
   //   @Column(name = "welcomemessage") <-- column names, if divergerande between class and db
   @Column(name = "welcomemessage")
   private String welcomeMessage;
   private String code;

   /**
    * Hibernate (JPA) needs it.
    */

   @SuppressWarnings("unused")
   public Lang()
   {
   }


   public Lang(Integer id, String welcomeMessage, String code)
   {
      this.id = id;
      this.welcomeMessage = welcomeMessage;
      this.code = code;
   }

   public Integer getId()
   {
      return id;
   }

   public String getWelcomeMessage()
   {
      return welcomeMessage;
   }

   public void setWelcomeMessage(String welcomeMessage)
   {
      this.welcomeMessage = welcomeMessage;
   }

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }
}
