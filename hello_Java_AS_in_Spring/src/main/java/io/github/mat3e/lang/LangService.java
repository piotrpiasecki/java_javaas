package io.github.mat3e.lang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class LangService
{
   private LangRepository repository;

//   @Autowired
   LangService(LangRepository repository)
   {
      this.repository = repository;
   }

      List<LangDTO> findAll()
      {
         return repository
            .findAll()
            .stream()
            .map(LangDTO::new)
            .collect(toList());
      }
}
