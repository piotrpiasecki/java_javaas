package io.github.mat3e.hello;

import io.github.mat3e.lang.Lang;
import io.github.mat3e.lang.LangRepository;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class HelloServiceTest
{
   private static final String WELCOME = "Hello";
   private static final String FALLBACK_ID_WELCOME = "Hello";

   @Test
   public void test_prepareGreeting_nullName_returnsGreetingWithFallback() throws Exception
   {
      // given
      LangRepository mockRepository = alwaysReturningHelloRepository();
      HelloService SUT = new HelloService(mockRepository);

      // when
      String result = SUT.prepareGreeting(null, -1);

      // then
      assertEquals(WELCOME + " " + HelloService.FALLBACK_NAME + "!", result);
   }

   @Test
   public void test_prepareGreeting_name_returnsGreetingWithName() throws Exception
   {
      // given
      HelloService SUT = new HelloService();
      String name = "test";

      // when
      String result = SUT.prepareGreeting(name, -1);

      // then
      assertEquals(WELCOME + " " + name + "!", result);
   }

   @Test
   public void test_prepareGreeting_nullLang_returnsGreetingWithFallbackIdLang() throws Exception
   {
      // given
      LangRepository mockRepository = fallbackLangIdRepository();
      HelloService SUT = new HelloService(mockRepository);

      // when
      String result = SUT.prepareGreeting(null, null);

      // then
      assertEquals(FALLBACK_ID_WELCOME + " " + HelloService.FALLBACK_NAME + "!", result);

   }

/*   @Test
   public void test_prepareGreeting_textLang_returnsGreetingWithFallbackIdLang() throws Exception
   {
      // given
      LangRepository mockRepository = fallbackLangIdRepository();
      HelloService SUT = new HelloService(mockRepository);

      // when
      String result = SUT.prepareGreeting(null, "abc");

      // then
      assertEquals(FALLBACK_ID_WELCOME + " " + HelloService.FALLBACK_NAME + "!", result);
   }*/

   @Test
   public void test_prepareGreeting_nonExistingLang_returnsGreetingWithFallbackLang() throws Exception
   {
      // given
      LangRepository mockRepository = nullReturningRepository();
      HelloService SUT = new HelloService(mockRepository);

      // when
      String result = SUT.prepareGreeting(null, -1);

      // then
      assertEquals(HelloService.FALLBACK_LANG.getWelcomeMessage() +
         " " + HelloService.FALLBACK_NAME + "!", result);
   }

   public LangRepository nullReturningRepository()
   {
      return new LangRepository()
      {
         @Override
         public Optional<Lang> findById(Integer id)
         {
            return Optional.empty();
         }
      };
   }

   public LangRepository fallbackLangIdRepository()
   {
      return new LangRepository()
      {
         @Override
         public Optional<Lang> findById(Integer id)
         {
            if (id.equals(HelloService.FALLBACK_LANG.getId()))
            {
               return Optional.of(new Lang(null, FALLBACK_ID_WELCOME, null));
            }
            return Optional.empty();
         }
      };
   }

   public LangRepository alwaysReturningHelloRepository() // static?
   {
      return new LangRepository()
      {
         @Override
         public Optional<Lang> findById(Integer id)
         {
            return Optional.of(new Lang(null, WELCOME, null));
         }
      };
   }
}