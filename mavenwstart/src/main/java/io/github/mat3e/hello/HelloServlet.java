package io.github.mat3e.hello;

import io.github.mat3e.lang.Lang;
import io.github.mat3e.lang.LangRepository;
import io.github.mat3e.lang.LangService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


@WebServlet(name = "Hello", urlPatterns = {"/api"})
public class HelloServlet extends HttpServlet
{
   private static final String NAME_PARAM = "name";
   private static final String LANG_PARAM = "languages";
   private final Logger logger = LoggerFactory.getLogger(HelloServlet.class);

   private HelloService service;

   /**
    * Servlet container needs it.
    */
   @SuppressWarnings("unused")
   public HelloServlet()
   {
      this(new HelloService());
//      this.service = new HelloService();
   }

   HelloServlet(HelloService service)
   {
      this.service = service;
   }

   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
   {
      String name = req.getParameter(NAME_PARAM);
      String lang = req.getParameter(LANG_PARAM);
      Integer langId = null;

      try
      {
         langId = Integer.valueOf(lang);
      }
      catch (NumberFormatException e)
      {
         logger.warn("Non-numeric language id used: " + lang);
      }

      logger.info("Got request with parameters " + req.getParameterMap());
      resp.getWriter().write(service.prepareGreeting(name, langId));

      /* OPTIONALLY
      String name = req.getParameter(NAME_PARAM);
      String greeting = service.prepareGreeting(name);
      resp.getWriter().write(greeting);*/
   }
}
