package io.github.mat3e.todo;

import io.github.mat3e.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Optional;

public class TodoRepository
{
   public Todo addTodo(Todo newTodo)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      Transaction transaction = session.beginTransaction();

      session.persist(newTodo);
      session.flush();

      transaction.commit();
      session.close();
      return newTodo;
   }

   List<Todo> findAll()
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      Transaction transaction = session.beginTransaction();

      List<Todo> result = session.createQuery("from Todo", Todo.class).list();

      transaction.commit();
      session.close();
      return result;
   }

   public Optional<Todo> toggleTodo(Integer id)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      Transaction transaction = session.beginTransaction();

      Todo result = session.get(Todo.class, id);
      boolean done = result.isDone();
      result.setDone(!done);

      transaction.commit();
      session.close();
      return Optional.ofNullable(result);
   }

   public Optional<Todo> findById(Integer id)
   {
      Session session = HibernateUtil.getSessionFactory().openSession();
      Transaction transaction = session.beginTransaction();

      Todo result = session.get(Todo.class, id);

      transaction.commit();
      session.close();
      return Optional.ofNullable(result);
   }
}
