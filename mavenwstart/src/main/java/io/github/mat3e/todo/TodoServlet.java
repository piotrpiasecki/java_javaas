package io.github.mat3e.todo;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.mat3e.lang.LangService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "Todo", urlPatterns = {"/api/todos/*"})
public class TodoServlet extends HttpServlet
{
   private final Logger logger = LoggerFactory.getLogger(TodoServlet.class);

   private ObjectMapper mapper;
   private TodoRepository repository;
   //   private TodoService service;
   /**
    * Servlet container needs it.
    */

   @SuppressWarnings("unused")
   public TodoServlet()
   {
      this(new TodoRepository(), new ObjectMapper());
//      this(new TodoService(), new ObjectMapper());
   }

   TodoServlet(TodoRepository repository, ObjectMapper mapper)
//   TodoServlet(TodoService service, ObjectMapper mapper)
   {
      this.repository = repository;
//      this.service = service;
      this.mapper = mapper;
   }

   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
   {
      logger.info("Got request with parameters " + req.getParameterMap());
      resp.setContentType("application/json;charset=UTF-8");
      mapper.writeValue(resp.getOutputStream(), repository.findAll());
//      mapper.writeValue(resp.getOutputStream(), service.findAll());
   }

   @Override
   protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
   {
      String pathInfo = req.getPathInfo().substring(1);
      Integer path = null;

      try
      {
         path = Integer.valueOf(pathInfo);
      }
      catch (NumberFormatException e)
      {
         logger.warn("Non-numeric todo id used: " + pathInfo);
      }

      logger.info("Got request with parameters " + req.getParameterMap());
      repository.toggleTodo(path);
//      logger.info(path.toString() + " " + repository.findById(path).get().isDone());
      resp.setContentType("application/json;charset=UTF-8");
      mapper.writeValue(resp.getOutputStream(), repository.findAll());
   }

   @Override
   protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
   {
      repository.addTodo(
              mapper.readValue(req.getInputStream(), Todo.class));
      resp.setContentType("application/json;charset=UTF-8");
      mapper.writeValue(resp.getOutputStream(), repository.findAll());
   }
}
